package com.itau.smallbusinessplace.service;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JWTService {
	String key = "S3nh4d34c3ss04l0j1nh4";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String username) {
		return JWT.create().withClaim("usernome", username).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("usernome").asString();
		}catch (Exception e) {
			return null;
		}
	}
}
