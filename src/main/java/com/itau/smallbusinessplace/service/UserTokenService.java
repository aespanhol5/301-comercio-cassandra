package com.itau.smallbusinessplace.service;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.itau.smallbusinessplace.models.Usuario;
import com.itau.smallbusinessplace.repository.UsuarioRepository;

@Service
public class UserTokenService {
	
	@Autowired
	JWTService jwtService;
	
	@Autowired
	UsuarioRepository usuarioRepository;

	public Usuario getUserByToken(HttpServletRequest request) {
		
		String token = request.getHeader("Authorization");
		
		token = token.replace("Bearer ", "");
		
		String username = jwtService.validarToken(token);
		
		if(username == null) {
			return new Usuario();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByUsername(username);
		
		if(!usuarioOptional.isPresent()) {
			return new Usuario();
		}
		
		return usuarioOptional.get();
	}
}
