package com.itau.smallbusinessplace.models;

import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Usuario extends Login {

	private String nome;
	private String email;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
