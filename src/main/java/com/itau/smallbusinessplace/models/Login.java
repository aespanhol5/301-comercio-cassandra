package com.itau.smallbusinessplace.models;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

public class Login {
	
	@PrimaryKey
	private String username;
	private String senha;
	

	public String getUsername() {
		return username;
	}
	public void setUsername(String usernome) {
		this.username = usernome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
