package com.itau.smallbusinessplace.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.google.common.base.Optional;
import com.itau.smallbusinessplace.models.Login;
import com.itau.smallbusinessplace.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, UUID> {
	public Optional<Usuario> findByUsername(String username);
}
