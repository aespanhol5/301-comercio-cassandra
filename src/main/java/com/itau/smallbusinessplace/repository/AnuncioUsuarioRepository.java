package com.itau.smallbusinessplace.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.google.common.base.Optional;
import com.itau.smallbusinessplace.models.AnuncioUsuario;

public interface AnuncioUsuarioRepository extends CrudRepository<AnuncioUsuario, UUID> {
	public Optional<AnuncioUsuario> findByUsername(String username);
}