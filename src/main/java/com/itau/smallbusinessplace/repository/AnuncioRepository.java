package com.itau.smallbusinessplace.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.smallbusinessplace.models.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, UUID> {

}
