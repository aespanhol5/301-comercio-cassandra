package com.itau.smallbusinessplace.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Optional;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import com.itau.smallbusinessplace.models.Login;
import com.itau.smallbusinessplace.models.Usuario;
import com.itau.smallbusinessplace.repository.UsuarioRepository;
import com.itau.smallbusinessplace.service.JWTService;
import com.itau.smallbusinessplace.service.PasswordService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	JWTService jwtService;
	
	@Autowired
	PasswordService passwordService;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> getUsuarios(){
		return usuarioRepository.findAll();
	}

	@RequestMapping(path="/criar", method=RequestMethod.POST)
	public ResponseEntity<?> criarUsuario(@RequestBody Usuario usuario) {
		usuario.setSenha(passwordService.gerarHash(usuario.getSenha()));
		return ResponseEntity.status(201).body(usuarioRepository.save(usuario));
	}
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public ResponseEntity<?> logarUsuario(@RequestBody Login usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUsername(usuario.getUsername());
		
		if(!usuarioBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Usuario usuarioBanco = usuarioBancoOptional.get(); 
		
		if(passwordService.verificarHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");			
			
			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();

	}
	
	@RequestMapping(path="/verificar")
	public ResponseEntity<?> verificarToken(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		
		token = token.replace("Bearer ", "");
		
		String username = jwtService.validarToken(token);
		
		if(username == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByUsername(username);
		
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		
		return ResponseEntity.ok().build();
	}

	
	
	
}
