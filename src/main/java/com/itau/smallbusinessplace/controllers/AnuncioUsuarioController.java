package com.itau.smallbusinessplace.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.smallbusinessplace.models.Usuario;
import com.itau.smallbusinessplace.repository.AnuncioUsuarioRepository;
import com.itau.smallbusinessplace.service.UserTokenService;

@RestController
@RequestMapping("/anuncios")
public class AnuncioUsuarioController {

	@Autowired
	UserTokenService userTokenService;
	
	@Autowired
	AnuncioUsuarioRepository anuncioUsuarioRepository;

	@RequestMapping("/usuario")
	public ResponseEntity<?> getAnuncios(HttpServletRequest request){
		
		Usuario usuario = userTokenService.getUserByToken(request);
		
		return ResponseEntity.status(200).body(anuncioUsuarioRepository.findByUsername(usuario.getUsername()));
	}
}
