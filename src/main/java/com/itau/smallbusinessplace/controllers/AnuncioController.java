package com.itau.smallbusinessplace.controllers;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.smallbusinessplace.models.Anuncio;
import com.itau.smallbusinessplace.models.AnuncioUsuario;
import com.itau.smallbusinessplace.models.Usuario;
import com.itau.smallbusinessplace.repository.AnuncioRepository;
import com.itau.smallbusinessplace.repository.AnuncioUsuarioRepository;
import com.itau.smallbusinessplace.service.UserTokenService;

@RestController
@RequestMapping("/anuncio")
public class AnuncioController {

	@Autowired
	AnuncioRepository anuncioRepository;
	
	@Autowired
	AnuncioUsuarioRepository anuncioUsuarioRepository;
	
	@Autowired
	UserTokenService userTokenService;

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> anunciar(@RequestBody Anuncio anuncio, HttpServletRequest request) {
		anuncio.setIdAnuncio(UUID.randomUUID());
		
		Usuario usuario = userTokenService.getUserByToken(request);
		anuncio.setUsuario(usuario.getNome());
		anuncio.setUsername(usuario.getUsername());
		anuncio.setEmail(usuario.getEmail());
		
		AnuncioUsuario anuncioUsuario = new AnuncioUsuario();
		
		anuncioUsuario.setIdAnuncio(anuncio.getIdAnuncio());
		anuncioUsuario.setTitulo(anuncio.getTitulo());
		anuncioUsuario.setDescricao(anuncio.getDescricao());
		anuncioUsuario.setPreco(anuncio.getPreco());
		anuncioUsuario.setData(anuncio.getData());
		anuncioUsuario.setUsuario(anuncio.getUsuario());
		anuncioUsuario.setUsername(anuncio.getUsername());
		anuncioUsuario.setEmail(anuncio.getEmail());
		
		anuncioUsuarioRepository.save(anuncioUsuario);
		
		return ResponseEntity.status(201).body(anuncioRepository.save(anuncio));
	}

	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> getAnuncio() {
		return anuncioRepository.findAll();
	}
	
}
